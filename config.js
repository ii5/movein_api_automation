/*jshint -W079 */
var FormData = require('form-data');
var path = require('path');
var fs = require('fs');
var frisby = require('./lib/frisby');

var serverName = 'http://10.50.249.15',
    port = '8081',
    folderName = 'movein-rest',
    version = 'v1';


module.exports = {
    FormData: FormData,
    path: path,
    fs: fs,
    frisby: frisby,
	url: serverName + ":" + port + "/" + folderName + "/" + version,
	makeFormData: function(obj) {
        var formData = new FormData();
        Object.keys(obj).forEach(function(key) {
         
            formData.append(key, obj[key]);
        });

        return formData;
    }
};
