var config = require('../../config');
var accountList = require('../../Data/account');
var mobileList = require('../../Data/mobile');
var path = require('path');
var fs = require('fs');
var code , token ;


describe('Generating New OTP for signin And Than Checking Valid And Invalid Data For upload Media', function() {
	it('New Otp Generate For Signin And Attempt To Upload Media Successfully', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				imageformvalues = {
					type: "photo"
				};
				var imageformData = {};
				imageformData = config.makeFormData(imageformvalues);
					var logoPath = path.resolve(__dirname, '../../Data/images/Penguins.jpg');
					imageformData.append('photo', fs.createReadStream(logoPath), {
                    knownLength: fs.statSync(logoPath).size
                    });
				config.frisby.create('Uploading Media Successfully')
					.post (config.url + '/upload',
						imageformData,
							{
							json: false,
							headers: {
								'content-type' : 'multipart/form-data; boundary=' + imageformData.getBoundary(),
								'content-length' : imageformData.getLengthSync(),
								'Authorization': "bearer " + token
								}})
					.expectStatus(200)
					.expectHeaderContains('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
    });
	it('New Otp Generate For Signin And Attempt To Upload Media With Size More Than 2Mb', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
			.timeout(30000)
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.timeout(30000)
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				imageformvalues = {
					type: "photo"
				};
				var imageformData = {};
				imageformData = config.makeFormData(imageformvalues);
					var logoPath = path.resolve(__dirname, '../../Data/images/1.jpg');
					imageformData.append('photo', fs.createReadStream(logoPath), {
                    knownLength: fs.statSync(logoPath).size
                    });
				config.frisby.create('Uploading Media With Size More Than 2Mb')
					.post (config.url + '/upload',
						imageformData,
							{
							json: false,
							headers: {
								'content-type' : 'multipart/form-data; boundary=' + imageformData.getBoundary(),
								'content-length' : imageformData.getLengthSync(),
								'Authorization': "bearer " + token
								}})
					.expectStatus(400)
					.expectHeaderContains('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
    });
	it('New Otp Generate For Signin And Attempt To Upload Media With Different Extention (except .jpg and .png)', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
			.timeout(30000)
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.timeout(30000)
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				imageformvalues = {
					type: "photo"
				};
				var imageformData = {};
				imageformData = config.makeFormData(imageformvalues);
					var logoPath = path.resolve(__dirname, '../../Data/images/AS.tiff');
					imageformData.append('photo', fs.createReadStream(logoPath), {
                    knownLength: fs.statSync(logoPath).size
                    });
				config.frisby.create('Uploading Media Different Extention (except .jpg and .png)')
					.post (config.url + '/upload',
						imageformData,
							{
							json: false,
							headers: {
								'content-type' : 'multipart/form-data; boundary=' + imageformData.getBoundary(),
								'content-length' : imageformData.getLengthSync(),
								'Authorization': "bearer " + token
								}})
					.expectStatus(200)
					.expectHeaderContains('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
    });
})