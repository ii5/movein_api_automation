var config = require('../../config');
var accountList = require('../../Data/account');
var mobileList = require('../../Data/mobile');
var extras = require('../../Data/extra');
var code , token ;

describe('Generating New OTP for SignIn And Adding A New Property Successfully', function() {
	it('Adding A New Property With Valid Data', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				config.frisby.create('Adding A New Property With Valid Data')
					.post (config.url + '/property_listings',
						{"property_listing":
							{
								"property_type": "residential" ,
								"property_sub_type":"apartment",
								"area":"3452",
								"commission_type": "via",
								"description": "A Very good property, must got for it, nice view from hall",
								"rooms": "1bhk",
								"sale_price": "46700000",
								"rent_price": "150000",
								"location_id": "888",
								"property_market": "secondary",
								"is_hot": "false",
								"amenities": [
									"3",
									"5"
								]
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(200)
					.expectHeader('content-type','application/json')
					.inspectJSON()
					.expectJSONTypes(
						{
						"result": {
							"code": String,
							"message": String,
							"is_fatal": Boolean
							},
							"data": {
							"property_listing_id": String,
							"short_reference": String
							}
						})
					.toss();
				})
			.toss();
			})
        .toss();
		});
	})