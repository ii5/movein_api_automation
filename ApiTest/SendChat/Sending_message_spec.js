var config = require('../../config');
var accountList = require('../../Data/account');
var mobileList = require('../../Data/mobile');
var extras = require('../../Data/extra');
var path = require('path');
var fs = require('fs');
var code , token , media ;


describe('Generating New OTP for signin And Sending Messages With Invalid Data', function() {
	it('Sending Message With Blank Type Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				config.frisby.create('Sending Message With Blank Type Field')
					.post (config.url + '/chat_messages',
						{"chat_message":
							{
								"text": "media" ,
								"type":"   ",
								"to_account_id": accountList.accountId1
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(400)
					.expectHeader('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
		});
	it('Sending Message With No Type Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				config.frisby.create('Sending Message With No Type Field')
					.post (config.url + '/chat_messages',
						{"chat_message":
							{
								"text": "media" ,
								
								"to_account_id": accountList.accountId1
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(400)
					.expectHeader('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
		});
	it('Sending Message With Invalid Type Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				config.frisby.create('Sending Message With Invalid Type Field')
					.post (config.url + '/chat_messages',
						{"chat_message":
							{
								"text": "media" ,
								"type":"yer&^",
								"to_account_id": accountList.accountId1
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(400)
					.expectHeader('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
		});
	it('Sending Message With Blank "to_account_id" Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				config.frisby.create('Sending Message With Blank "to_account_id" Field')
					.post (config.url + '/chat_messages',
						{"chat_message":
							{
								"text": "media" ,
								"type":"text",
								"to_account_id": "  "
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(400)
					.expectHeader('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
		});
	it('Sending Message With Invalid "to_account_id" Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				config.frisby.create('Sending Message With Invalid "to_account_id" Field')
					.post (config.url + '/chat_messages',
						{"chat_message":
							{
								"text": "media" ,
								"type":"text",
								"to_account_id": "67^&*^&*^&*23786423"
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(404)
					.expectHeader('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
		});
	it('Sending Message With No "to_account_id" Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				config.frisby.create('Sending Message With No "to_account_id" Field')
					.post (config.url + '/chat_messages',
						{"chat_message":
							{
								"text": "media" ,
								"type":"text"
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(400)
					.expectHeader('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
		});
	it('Sending Message From An Unregistered Account', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile11
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile11,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				config.frisby.create('Sending Message From An Unregistered Account')
					.post (config.url + '/chat_messages',
						{"chat_message":
							{
								"text": "media" ,
								"type":"text",
								"to_account_id": accountList.account1
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(403)
					.expectHeader('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
		});
})