var config = require('../../config');
var accountList = require('../../Data/account');
var mobileList = require('../../Data/mobile');
var extras = require('../../Data/extra');
var path = require('path');
var fs = require('fs');
var code , token , media ;


describe('Generating New OTP for signin And Send Text Messages With Valid And Invalid Data', function() {
	it('New Otp Generate For Signin And Send Text Message Successfully', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				config.frisby.create('Sending Text Successfully')
					.post (config.url + '/chat_messages',
						{"chat_message":
							{
								"text": "media" ,
								"type":"text",
								"to_account_id":"2556090837284082"
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(200)
					.expectHeader('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
		});
	it('Sending Text Message With Blank Text Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				config.frisby.create('Sending Text Message With Blank Text Field')
					.post (config.url + '/chat_messages',
						{"chat_message":
							{
								"text": "     " ,
								"type":"text",
								"to_account_id":"2556090837284082"
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(400)
					.expectHeader('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
    });
	it('Sending Text Message With No Text Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				config.frisby.create('Sending Text Message With No Text Field')
					.post (config.url + '/chat_messages',
						{"chat_message":
							{
								
								"type":"text",
								"to_account_id":"2556090837284082"
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(400)
					.expectHeader('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
    });
	it('Sending Text Message With More Than 4000 Characters', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				config.frisby.create('Sending Text Message With More Than 4000 Characters')
					.post (config.url + '/chat_messages',
						{"chat_message":
							{
								"text": extras.extra1 ,
								"type":"text",
								"to_account_id":"2556090837284082"
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(400)
					.expectHeader('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
    });
	it('Sending Photo While Type is "Text"', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				imageformvalues = {
					type: "photo"
				};
				var imageformData = {};
				imageformData = config.makeFormData(imageformvalues);
					var logoPath = path.resolve(__dirname, '../../Data/images/Penguins.jpg');
					imageformData.append('photo', fs.createReadStream(logoPath), {
                    knownLength: fs.statSync(logoPath).size
                    });
				config.frisby.create('Uploading Media Successfully')
					.post (config.url + '/upload',
						imageformData,
							{
							json: false,
							headers: {
								'content-type' : 'multipart/form-data; boundary=' + imageformData.getBoundary(),
								'content-length' : imageformData.getLengthSync(),
								'Authorization': "bearer " + token
								}})
					.expectStatus(200)
					.expectHeaderContains('content-type','application/json')
					.afterJSON(function(jsonData){
						media = jsonData.data;
				config.frisby.create('Sending Photo While Type is "Text"')
					.post (config.url + '/chat_messages',
						{"chat_message":
							{
								"photo": media ,
								"type":"text",
								"to_account_id":"2556090837284082"
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(400)
					.expectHeader('content-type','application/json')
					.inspectJSON()
					.toss();
					})
					.toss();
				})
			.toss();
			})
        .toss();
    });
})
