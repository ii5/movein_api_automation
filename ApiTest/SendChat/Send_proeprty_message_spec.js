var config = require('../../config');
var accountList = require('../../Data/account');
var mobileList = require('../../Data/mobile');
var extras = require('../../Data/extra');
var path = require('path');
var fs = require('fs');
var code , token , media ;


describe('Generating New OTP for signin And Send Property With Invalid Data', function() {
	it('Sending Text Message With Blank Property Reference Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				config.frisby.create('Sending Message With Blank Property Reference Field')
					.post (config.url + '/chat_messages',
						{"chat_message":
							{
								"property_listing_short_reference": "   " ,
								"type":"property_listing",
								"to_account_id":"2556090837284082"
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(200)
					.expectHeader('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
		});
	it('Sending Message With No Property Reference Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				config.frisby.create('Sending Message With No Property Reference Field')
					.post (config.url + '/chat_messages',
						{"chat_message":
							{
								
								"type":"property_listing",
								"to_account_id":"2556090837284082"
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(400)
					.expectHeader('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
    });
	it('Sending Message With Invalid Property Reference Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				config.frisby.create('Sending Message With Invalid Property Reference Field')
					.post (config.url + '/chat_messages',
						{"chat_message":
							{
								"property_listing_short_reference": "ABCDE" ,
								"type":"property_listing",
								"to_account_id":"2556090837284082"
							}
							},
						{json : true,
						headers :
							{
							"Content-Type" : "application/json",
							"Authorization" : "bearer " + token
							}
						})
					.expectStatus(404)
					.expectHeader('content-type','application/json')
					.toss();
				})
			.toss();
			})
        .toss();
    });
	
})
