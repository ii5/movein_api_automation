var config = require('../../config');
var mobileList = require('../../Data/mobile');
var code = "", token = "" ;


describe('Registration : All Invalid Testcases For Location Field', function() {
	it('Attempt Registration With Blank Location Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile17
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile17,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json:true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.expectJSON	(
					{
						"result":
						{
							"code": String,
							"message": String,
							"is_fatal": Boolean
						},
						"data":
						{
							"initial_sync_index": Number,
							"push_channel": String,
							"push_key" : String,
							"access_token": String,
							"account_id": String,
							"scope" : String,
							"token_type":String
						}
					})
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
			config.frisby.create('Attempt Registration With Blank Locations Field')
			.post(config.url + '/registration',
			{
				"account" :
					{
						"first_name" : "Prabhudatta",
						"last_name" : "Nayak",
						"locations" : [  ]
					}
			},
				{
				json: true,
				headers :
						{
						"Content-Type" : "application/json",
						"Authorization" : "bearer " + token
						}
				})
			.expectStatus(400)
			.expectHeader('content-type','application/json')
			.inspectJSON()
			.toss();
			})
		.toss();
		})
    .toss();
    });
	it('Attempt Registration With No Location Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile17
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile17,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json:true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.expectJSON	(
					{
						"result":
						{
							"code": String,
							"message": String,
							"is_fatal": Boolean
						},
						"data":
						{
							"initial_sync_index": Number,
							"push_channel": String,
							"push_key" : String,
							"access_token": String,
							"account_id": String,
							"scope" : String,
							"token_type":String
						}
					})
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
			config.frisby.create('Attempt Registration With No Location Field')
			.post(config.url + '/registration',
			{
				"account" :
					{
						
						"First_name" : "Prabhudatta",
						"last_name" : "Nayak"
					}
			},
				{
				json: true,
				headers :
						{
						"Content-Type" : "application/json",
						"Authorization" : "bearer " + token
						}
				})
			.expectStatus(400)
			.expectHeader('content-type','application/json')
			.inspectJSON()
			.toss();
			})
		.toss();
		})
    .toss();
    });
	it('Attempt Registration With More Than 10 Locations Id', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile17
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile17,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json:true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.expectJSON	(
					{
						"result":
						{
							"code": String,
							"message": String,
							"is_fatal": Boolean
						},
						"data":
						{
							"initial_sync_index": Number,
							"push_channel": String,
							"push_key" : String,
							"access_token": String,
							"account_id": String,
							"scope" : String,
							"token_type":String
						}
					})
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
			config.frisby.create('Attempt Registration With More Than 10 Locations Id')
			.post(config.url + '/registration',
			{
				"account" :
					{
						"first_name" : "Prabhudatta",
						"last_name" : "Nayak",
						"locations" : [ "03750ab7-5e72-48e3-b8d3-cc34bbf36ba7",
										"4e44926d-9e43-4db2-bf31-7fdc05856590",
										"db30ecf4-2106-4029-a9c8-6057eefb1f6a",
										"716a8a14-2727-4498-b28b-ef54ab343cad",
										"5ca5548a-00d1-4449-8532-1447e33ea778",
										"3638506d-fff7-4e78-9f19-8d60977fefc0",
										"20991811-a66c-48ee-b10d-0c422695de5d",
										"82ceea6e-7621-44ba-9e87-2b5f6bbd50c7",
										"9415310d-6350-4e46-9186-98d38b64a66a",
										"ba6ea046-a394-4eae-a99c-ef9e751e3860",
										"e807bb82-f2f8-41dc-a390-38f60868174b "
									]
					}
			},
				{
				json: true,
				headers :
						{
						"Content-Type" : "application/json",
						"Authorization" : "bearer " + token
						}
				})
			.expectStatus(400)
			.expectHeader('content-type','application/json')
			.inspectJSON()
			.toss();
			})
		.toss();
		})
    .toss();
    });
	it('Attempt Registration With 1 Invalid Locations Id', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile17
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile17,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json:true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.expectJSON	(
					{
						"result":
						{
							"code": String,
							"message": String,
							"is_fatal": Boolean
						},
						"data":
						{
							"initial_sync_index": Number,
							"push_channel": String,
							"push_key" : String,
							"access_token": String,
							"account_id": String,
							"scope" : String,
							"token_type":String
						}
					})
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
			config.frisby.create('Attempt Registration With 1 Invalid Locations Id')
			.post(config.url + '/registration',
			{
				"account" :
					{
						"first_name" : "Prabhudatta",
						"last_name" : "Nayak",
						"locations" : [ "03750ab7-5e72-48e3-b8d3-cc34bbf36ba7",
										"4e44926d-9e43-4db2-bf31-7fdc05856590",
										"db30ecf4-2106-4029-a9c8-6057eefb1f6a",
										"716a8a14-2727-4498-b28b-ef54ab343cad",
										"5ca5548a-00d1-4449-8532-1447e33ea778",
										"3638506d-fff7-4e78-9f19-8d60977fefu9",
										"20991811-a66c-48ee-b10d-0c422695de5d",
										"82ceea6e-7621-44ba-9e87-2b5f6bbd50c7",
										"9415310d-6350-4e46-9186-98d38b64a66a",
										"e807bb82-f2f8-41dc-a390-38f60868174b "
									]
					}
			},
				{
				json: true,
				headers :
						{
						"Content-Type" : "application/json",
						"Authorization" : "bearer " + token
						}
				})
			.expectStatus(404)
			.expectHeader('content-type','application/json')
			.inspectJSON()
			.toss();
			})
		.toss();
		})
    .toss();
    });
})