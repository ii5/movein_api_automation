var config = require('../../config');
var mobileList = require('../../Data/mobile');
var code = "", token = "" ;


describe('Registration : All Invalid Testcases For Firstname Field', function() {
	it('Attempt Registration With Blank Firstname Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile17
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile17,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json:true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.expectJSON	(
					{
						"result":
						{
							"code": String,
							"message": String,
							"is_fatal": Boolean
						},
						"data":
						{
							"initial_sync_index": Number,
							"push_channel": String,
							"push_key" : String,
							"access_token": String,
							"account_id": String,
							"scope" : String,
							"token_type":String
						}
					})
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
			config.frisby.create('Attempt Registration With Blank Firstname Field')
			.post(config.url + '/registration',
			{
				"account" :
					{
						"first_name" : "   ",
						"last_name" : "Nayak",
						"locations" : ["03750ab7-5e72-48e3-b8d3-cc34bbf36ba7"]
					}
			},
				{
				json: true,
				headers :
						{
						"Content-Type" : "application/json",
						"Authorization" : "bearer " + token
						}
				})
			.expectStatus(400)
			.expectHeader('content-type','application/json')
			.toss();
			})
		.toss();
		})
    .toss();
    });
	it('Attempt Registration With No Firstname Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile17
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile17,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json:true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.expectJSON	(
					{
						"result":
						{
							"code": String,
							"message": String,
							"is_fatal": Boolean
						},
						"data":
						{
							"initial_sync_index": Number,
							"push_channel": String,
							"push_key" : String,
							"access_token": String,
							"account_id": String,
							"scope" : String,
							"token_type":String
						}
					})
			.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
			config.frisby.create('Attempt Registration With No Firstname Field')
			.post(config.url + '/registration',
			{
				"account" :
					{
						
						"last_name" : "Nayak",
						"locations" : ["03750ab7-5e72-48e3-b8d3-cc34bbf36ba7"]
					}
			},
				{
				json: true,
				headers :
						{
						"Content-Type" : "application/json",
						"Authorization" : "bearer " + token
						}
				})
			.expectStatus(400)
			.expectHeader('content-type','application/json')
			.toss();
			})
		.toss();
		})
    .toss();
    });
})