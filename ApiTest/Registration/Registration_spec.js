var config = require('../../config');
var formValues = { };
var signInFormValues = {};
var code , token ;
//var path = ('C:/Users/Public/Pictures/Sample Pictures');


describe('Generating New OTP for Successful Registration', function() {
    it('Signin With New Generated Otp And Attempt Registration valid Data', function () {
		signInFormValues = {
			mobile_number: "+916666666668"
		};
		var signInForm = {};
		signInForm = config.makeFormData(signInFormValues);
		var boundary = signInForm.getBoundary();
        var lengthSync = signInForm.getLengthSync();
        var mobile = signInFormValues.mobile_number;

   
        config.frisby.create ('OTP should Generate For Successful Signin')
			.post(config.url + '/request_otp',
            signInForm,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + boundary,
                  'content-length': lengthSync
                }
            })
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData) {
				code = jsonData.data.otp;
				mobile = jsonData.data.mobile_number;
				signInFormValues = {
					mobile_number: mobile,
					otp: code,
					grant_type: 'client_credentials',
					client_type: 'mobile',
					device_id: '1234',
					device_user_name: 'my device',
					device_model_name: 'iphone',
					device_platform: 'ios',
					device_os_version: 'ios8'
			};
			var signInFormData = {};
			signInFormData = config.makeFormData(signInFormValues);
				config.frisby.create ('Attempt For Successful login')
				.post (config.url + '/sign_in',
				signInFormData,
					{
						json: false,
						headers: {
							'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
							'content-length': signInFormData.getLengthSync()
						}
					}
				)
				.expectStatus(200)
				.afterJSON(function(jsonData){
					token = jsonData.data.access_token;
					registerFormValues = {
							first_name: 'Prabhudatta',
							last_name: 'Nayak',
							localities:"03750ab7-5e72-48e3-b8d3-cc34bbf36ba7",
							
						};
					var registerFormData = {};
					
					registerFormData = config.makeFormData(registerFormValues);

				config.frisby.create ('Attempt For Successful Registration')				
				.post(config.url + '/registration',
						registerFormData,
						{
						json: false,
							headers: {
								'content-type': 'multipart/form-data; boundary=' + registerFormData.getBoundary(),
								'content-length': registerFormData.getLengthSync(),
								'Authorization' : 'bearer ' + token
							}
						})
					.expectStatus(200)
					.inspectJSON()
					.toss();
					})
					.toss();
				})
		.toss();
	});
})