var config = require('../../config');
var accountList = require('../../Data/account');
var mobileList = require('../../Data/mobile');
var path = require('path');
var fs = require('fs');
var extras = require('../../Data/extra');
var formValues = { };
var signInFormValues = {};
var code , token ;


describe('Generating New OTP for SignIn And Sending Text Message With Invalid Property Reference', function() {
    it('Sending Text Message With Blank Property Reference Field', function () {
		signInFormValues = {
			mobile_number: mobileList.mobile1
		};
		var signInForm = {};
		signInForm = config.makeFormData(signInFormValues);
		var boundary = signInForm.getBoundary();
        var lengthSync = signInForm.getLengthSync();
        var mobile = signInFormValues.mobile_number;

   
        config.frisby.create ('OTP should Generate For Login')
			.post(config.url + '/request_otp',
            signInForm,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + boundary,
                  'content-length': lengthSync
                }
            })
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData) {
				code = jsonData.data.otp;
				mobile = jsonData.data.mobile_number;
				signInFormValues = {
					mobile_number: mobile,
					otp: code,
					grant_type: 'client_credentials',
					client_type: 'mobile',
					device_id: '1234',
					device_user_name: 'my device',
					device_model_name: 'iphone',
					device_platform: 'ios',
					device_os_version: 'ios8'
			};
			var signInFormData = {};
			signInFormData = config.makeFormData(signInFormValues);
				config.frisby.create ('Successful login')
				.post (config.url+'/sign_in',
				signInFormData,
					{
						json: false,
						headers: {
							'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
							'content-length': signInFormData.getLengthSync()
						}
					}
				)
				.expectStatus(200)
				.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				chatformvalues = {
					type: "property_listing",
					property_listing_short_reference: "    ",
					to_account_id: accountList.accountId3
				};
				var chatformData = {};
				chatformData = config.makeFormData(chatformvalues);
				config.frisby.create('Sending Text Message With Blank Property Reference Field')
					.post (config.url + '/chat_messages',
						chatformData,
							{
							json: false,
							headers: {
								'content-type' : 'multipart/form-data; boundary=' + chatformData.getBoundary(),
								'content-length' : chatformData.getLengthSync(),
								'Authorization': "bearer " + token
								}})
					.expectStatus(400)
					.expectHeaderContains('content-type','application/json')
					.toss();
				})
				.toss();
			})
		.toss();
		});
	it('Sending Text Message With No Property Reference Field', function () {
		signInFormValues = {
			mobile_number: mobileList.mobile1
		};
		var signInForm = {};
		signInForm = config.makeFormData(signInFormValues);
		var boundary = signInForm.getBoundary();
        var lengthSync = signInForm.getLengthSync();
        var mobile = signInFormValues.mobile_number;

   
        config.frisby.create ('OTP should Generate For Login')
			.post(config.url + '/request_otp',
            signInForm,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + boundary,
                  'content-length': lengthSync
                }
            })
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData) {
				code = jsonData.data.otp;
				mobile = jsonData.data.mobile_number;
				signInFormValues = {
					mobile_number: mobile,
					otp: code,
					grant_type: 'client_credentials',
					client_type: 'mobile',
					device_id: '1234',
					device_user_name: 'my device',
					device_model_name: 'iphone',
					device_platform: 'ios',
					device_os_version: 'ios8'
			};
			var signInFormData = {};
			signInFormData = config.makeFormData(signInFormValues);
				config.frisby.create ('Successful login')
				.post (config.url+'/sign_in',
				signInFormData,
					{
						json: false,
						headers: {
							'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
							'content-length': signInFormData.getLengthSync()
						}
					}
				)
				.expectStatus(200)
				.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				chatformvalues = {

					type: "property_listing",
					to_account_id: accountList.accountId3
				};
				var chatformData = {};
				chatformData = config.makeFormData(chatformvalues);
				config.frisby.create('Sending Text Message With No Property Reference Field')
					.post (config.url + '/chat_messages',
						chatformData,
							{
							json: false,
							headers: {
								'content-type' : 'multipart/form-data; boundary=' + chatformData.getBoundary(),
								'content-length' : chatformData.getLengthSync(),
								'Authorization': "bearer " + token
								}})
					.expectStatus(400)
					.expectHeaderContains('content-type','application/json')
					.toss();
				})
				.toss();
			})
		.toss();
		});
	it('Sending Text Message With Invalid Property Reference Field', function () {
		signInFormValues = {
			mobile_number: mobileList.mobile1
		};
		var signInForm = {};
		signInForm = config.makeFormData(signInFormValues);
		var boundary = signInForm.getBoundary();
        var lengthSync = signInForm.getLengthSync();
        var mobile = signInFormValues.mobile_number;

   
        config.frisby.create ('OTP should Generate For Login')
			.post(config.url + '/request_otp',
            signInForm,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + boundary,
                  'content-length': lengthSync
                }
            })
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData) {
				code = jsonData.data.otp;
				mobile = jsonData.data.mobile_number;
				signInFormValues = {
					mobile_number: mobile,
					otp: code,
					grant_type: 'client_credentials',
					client_type: 'mobile',
					device_id: '1234',
					device_user_name: 'my device',
					device_model_name: 'iphone',
					device_platform: 'ios',
					device_os_version: 'ios8'
			};
			var signInFormData = {};
			signInFormData = config.makeFormData(signInFormValues);
				config.frisby.create ('Successful login')
				.post (config.url+'/sign_in',
				signInFormData,
					{
						json: false,
						headers: {
							'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
							'content-length': signInFormData.getLengthSync()
						}
					}
				)
				.expectStatus(200)
				.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				chatformvalues = {
					type: "property_listing",
					property_listing_short_reference: "&^%&",
					to_account_id: accountList.accountId3
				};
				var chatformData = {};
				chatformData = config.makeFormData(chatformvalues);
				config.frisby.create('Sending Text Message With Invalid Property Reference Field')
					.post (config.url + '/chat_messages',
						chatformData,
							{
							json: false,
							headers: {
								'content-type' : 'multipart/form-data; boundary=' + chatformData.getBoundary(),
								'content-length' : chatformData.getLengthSync(),
								'Authorization': "bearer " + token
								}})
					.expectStatus(400)
					.expectHeaderContains('content-type','application/json')
					.inspectJSON()
					.toss();
				})
				.toss();
			})
		.toss();
		});
	it('Sending Text Message With Expired Property Reference code', function () {
			signInFormValues = {
				mobile_number: mobileList.mobile1
			};
			var signInForm = {};
			signInForm = config.makeFormData(signInFormValues);
			var boundary = signInForm.getBoundary();
	        var lengthSync = signInForm.getLengthSync();
	        var mobile = signInFormValues.mobile_number;

	   
	        config.frisby.create ('OTP should Generate For Login')
				.post(config.url + '/request_otp',
	            signInForm,
	            {
					json: false,
					headers: {
						'content-type': 'multipart/form-data; boundary=' + boundary,
						'content-length': lengthSync
					}
				})
				.expectStatus(200)
				.expectHeader('content-type','application/json')
				.afterJSON(function(jsonData) {
					code = jsonData.data.otp;
					mobile = jsonData.data.mobile_number;
					signInFormValues = {
						mobile_number: mobile,
						otp: code,
						grant_type: 'client_credentials',
						client_type: 'mobile',
						device_id: '1234',
						device_user_name: 'my device',
						device_model_name: 'iphone',
						device_platform: 'ios',
						device_os_version: 'ios8'
				};
				var signInFormData = {};
				signInFormData = config.makeFormData(signInFormValues);
					config.frisby.create ('Successful login')
					.post (config.url+'/sign_in',
					signInFormData,
						{
							json: false,
							headers: {
								'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
								'content-length': signInFormData.getLengthSync()
							}
						}
					)
					.expectStatus(200)
					.afterJSON(function(jsonData){
					token = jsonData.data.access_token;
					chatformvalues = {
						type: "property_listing",
						property_listing_short_reference: propertyList.property_exp,
						to_account_id: accountList.accountId3
					};
					var chatformData = {};
					chatformData = config.makeFormData(chatformvalues);
					config.frisby.create('Sending Text Message With Expired Property Reference Code')
						.post (config.url + '/chat_messages',
							chatformData,
								{
								json: false,
								headers: {
									'content-type' : 'multipart/form-data; boundary=' + chatformData.getBoundary(),
									'content-length' : chatformData.getLengthSync(),
									'Authorization': "bearer " + token
									}})
						.expectStatus(400)
						.expectHeaderContains('content-type','application/json')
						.toss();
					})
					.toss();
				})
			.toss();
			});
	it('Sending Text Message With Property ID (While User Do Not Have Registered Property)', function () {
		signInFormValues = {
			mobile_number: mobileList.mobile1
		};
		var signInForm = {};
		signInForm = config.makeFormData(signInFormValues);
		var boundary = signInForm.getBoundary();
        var lengthSync = signInForm.getLengthSync();
        var mobile = signInFormValues.mobile_number;

   
        config.frisby.create ('OTP should Generate For Login')
			.post(config.url + '/request_otp',
            signInForm,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + boundary,
                  'content-length': lengthSync
                }
            })
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData) {
				code = jsonData.data.otp;
				mobile = jsonData.data.mobile_number;
				signInFormValues = {
					mobile_number: mobile,
					otp: code,
					grant_type: 'client_credentials',
					client_type: 'mobile',
					device_id: '1234',
					device_user_name: 'my device',
					device_model_name: 'iphone',
					device_platform: 'ios',
					device_os_version: 'ios8'
			};
			var signInFormData = {};
			signInFormData = config.makeFormData(signInFormValues);
				config.frisby.create ('Successful login')
				.post (config.url+'/sign_in',
				signInFormData,
					{
						json: false,
						headers: {
							'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
							'content-length': signInFormData.getLengthSync()
						}
					}
				)
				.expectStatus(200)
				.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				chatformvalues = {
					type: "property_listing",
					property_listing_id: propertyList.exp_property,
					to_account_id: accountList.accountId3
				};
				var chatformData = {};
				chatformData = config.makeFormData(chatformvalues);
				config.frisby.create('Sending Text Message With Property ID (While User Do Not Have Registered Property)')
					.post (config.url + '/chat_messages',
						chatformData,
							{
							json: false,
							headers: {
								'content-type' : 'multipart/form-data; boundary=' + chatformData.getBoundary(),
								'content-length' : chatformData.getLengthSync(),
								'Authorization': "bearer " + token
								}})
					.expectStatus(400)
					.expectHeaderContains('content-type','application/json')
					.toss();
				})
				.toss();
			})
		.toss();
		});
	it('Sending Photo While Type is "proeprty_listing', function () {
			signInFormValues = {
				mobile_number: mobileList.mobile1
			};
			var signInForm = {};
			signInForm = config.makeFormData(signInFormValues);
			var boundary = signInForm.getBoundary();
	        var lengthSync = signInForm.getLengthSync();
	        var mobile = signInFormValues.mobile_number;

	   
	        config.frisby.create ('OTP should Generate For Login')
				.post(config.url + '/request_otp',
	            signInForm,
	            {
	                json: false,
	                headers: {
	                  'content-type': 'multipart/form-data; boundary=' + boundary,
	                  'content-length': lengthSync
	                }
	            })
	            .expectStatus(200)
				.expectHeader('content-type','application/json')
				.afterJSON(function(jsonData) {
					code = jsonData.data.otp;
					mobile = jsonData.data.mobile_number;
					signInFormValues = {
						mobile_number: mobile,
						otp: code,
						grant_type: 'client_credentials',
						client_type: 'mobile',
						device_id: '1234',
						device_user_name: 'my device',
						device_model_name: 'iphone',
						device_platform: 'ios',
						device_os_version: 'ios8'
				};
				var signInFormData = {};
				signInFormData = config.makeFormData(signInFormValues);
					config.frisby.create ('Successful login')
					.post (config.url+'/sign_in',
					signInFormData,
						{
							json: false,
							headers: {
								'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
								'content-length': signInFormData.getLengthSync()
							}
						}
					)
					.expectStatus(200)
					.afterJSON(function(jsonData){
					token = jsonData.data.access_token;
					chatformvalues = {
						type: "property_listing",
						to_account_id: accountList.accountId3
					};
					var chatformData = {};
					chatformData = config.makeFormData(chatformvalues);
						var logoPath = path.resolve(__dirname, '../../Data/images/AW.png');
						chatformData.append('photo', fs.createReadStream(logoPath), {
						knownLength: fs.statSync(logoPath).size
                        });

					config.frisby.create('Sending Photo While Type is "proeprty_listing" ')
						.post (config.url + '/chat_messages',
							chatformData,
								{
								json: false,
								headers: {
									'content-type' : 'multipart/form-data; boundary=' + chatformData.getBoundary(),
									'content-length' : chatformData.getLengthSync(),
									'Authorization': "bearer " + token
									}})
						.expectStatus(400)
						.inspectJSON()
						.expectHeaderContains('content-type','application/json')

						.toss();
					})
					.toss();
				})
			.toss();
			});

});
