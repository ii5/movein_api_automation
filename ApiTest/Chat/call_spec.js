var config = require('../../config');
var accountList = require('../../Data/account');
var mobileList = require('../../Data/mobile');
var propertyList = require('../../Data/property');
var formValues = { };
var signInFormValues = {};
var code , token ;


describe('Generating New OTP for SignIn And Sending Phone Call With Invalid values in ', function() {
    it('Sending Phone Call With Blank Property ID', function () {
		signInFormValues = {
			mobile_number: mobileList.mobile1
		};
		var signInForm = {};
		signInForm = config.makeFormData(signInFormValues);
		var boundary = signInForm.getBoundary();
        var lengthSync = signInForm.getLengthSync();
        var mobile = signInFormValues.mobile_number;

   
        config.frisby.create ('OTP should Generate For Login')
			.post(config.url + '/request_otp',
            signInForm,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + boundary,
                  'content-length': lengthSync
                }
            })
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData) {
				code = jsonData.data.otp;
				mobile = jsonData.data.mobile_number;
				signInFormValues = {
					mobile_number: mobile,
					otp: code,
					grant_type: 'client_credentials',
					client_type: 'mobile',
					device_id: '1234',
					device_user_name: 'my device',
					device_model_name: 'iphone',
					device_platform: 'ios',
					device_os_version: 'ios8'
			};
			var signInFormData = {};
			signInFormData = config.makeFormData(signInFormValues);
				config.frisby.create ('Successful login')
				.post (config.url+'/sign_in',
				signInFormData,
					{
						json: false,
						headers: {
							'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
							'content-length': signInFormData.getLengthSync()
						}
					}
				)
				.expectStatus(200)
				.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				chatformvalues = {
					type: "phone_call",
					property_listing_id: "    ",
					to_account_id: accountList.accountId5
				};
				var chatformData = {};
				chatformData = config.makeFormData(chatformvalues);
				config.frisby.create('Sending Phone Call With Blank Property ID')
					.post (config.url + '/chat_messages',
						chatformData,
							{
							json: false,
							headers: {
								'content-type' : 'multipart/form-data; boundary=' + chatformData.getBoundary(),
								'content-length' : chatformData.getLengthSync(),
								'Authorization': "bearer " + token
								}})
					.expectStatus(400)
					.expectHeaderContains('content-type','application/json')
					.toss();
				})
				.toss();
			})
		.toss();
		});
	it('Sending Phone Call With Invalid Property ID', function () {
		signInFormValues = {
			mobile_number: mobileList.mobile1
		};
		var signInForm = {};
		signInForm = config.makeFormData(signInFormValues);
		var boundary = signInForm.getBoundary();
        var lengthSync = signInForm.getLengthSync();
        var mobile = signInFormValues.mobile_number;

   
        config.frisby.create ('OTP should Generate For Login')
			.post(config.url + '/request_otp',
            signInForm,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + boundary,
                  'content-length': lengthSync
                }
            })
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData) {
				code = jsonData.data.otp;
				mobile = jsonData.data.mobile_number;
				signInFormValues = {
					mobile_number: mobile,
					otp: code,
					grant_type: 'client_credentials',
					client_type: 'mobile',
					device_id: '1234',
					device_user_name: 'my device',
					device_model_name: 'iphone',
					device_platform: 'ios',
					device_os_version: 'ios8'
			};
			var signInFormData = {};
			signInFormData = config.makeFormData(signInFormValues);
				config.frisby.create ('Successful login')
				.post (config.url+'/sign_in',
				signInFormData,
					{
						json: false,
						headers: {
							'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
							'content-length': signInFormData.getLengthSync()
						}
					}
				)
				.expectStatus(200)
				.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				chatformvalues = {

					type: "phone_call",
					property_listing_id: "^&$^$JHGJjhsd",
					to_account_id: accountList.accountId3
				};
				var chatformData = {};
				chatformData = config.makeFormData(chatformvalues);
				config.frisby.create('Sending Phone Call With Invalid Property ID')
					.post (config.url + '/chat_messages',
						chatformData,
							{
							json: false,
							headers: {
								'content-type' : 'multipart/form-data; boundary=' + chatformData.getBoundary(),
								'content-length' : chatformData.getLengthSync(),
								'Authorization': "bearer " + token
								}})
					.expectStatus(400)
					.expectHeaderContains('content-type','application/json')
					.toss();
				})
				.toss();
			})
		.toss();
		});
	it('Sending Phone Call With No Property ID Field', function () {
		signInFormValues = {
			mobile_number: mobileList.mobile1
		};
		var signInForm = {};
		signInForm = config.makeFormData(signInFormValues);
		var boundary = signInForm.getBoundary();
        var lengthSync = signInForm.getLengthSync();
        var mobile = signInFormValues.mobile_number;

   
        config.frisby.create ('OTP should Generate For Login')
			.post(config.url + '/request_otp',
            signInForm,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + boundary,
                  'content-length': lengthSync
                }
            })
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData) {
				code = jsonData.data.otp;
				mobile = jsonData.data.mobile_number;
				signInFormValues = {
					mobile_number: mobile,
					otp: code,
					grant_type: 'client_credentials',
					client_type: 'mobile',
					device_id: '1234',
					device_user_name: 'my device',
					device_model_name: 'iphone',
					device_platform: 'ios',
					device_os_version: 'ios8'
			};
			var signInFormData = {};
			signInFormData = config.makeFormData(signInFormValues);
				config.frisby.create ('Successful login')
				.post (config.url+'/sign_in',
				signInFormData,
					{
						json: false,
						headers: {
							'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
							'content-length': signInFormData.getLengthSync()
						}
					}
				)
				.expectStatus(200)
				.afterJSON(function(jsonData){
				token = jsonData.data.access_token;
				chatformvalues = {
					type: "phone_call",
					
					to_account_id: accountList.accountId3
				};
				var chatformData = {};
				chatformData = config.makeFormData(chatformvalues);
				config.frisby.create('Sending Phone Call With No Property ID Field')
					.post (config.url + '/chat_messages',
						chatformData,
							{
							json: false,
							headers: {
								'content-type' : 'multipart/form-data; boundary=' + chatformData.getBoundary(),
								'content-length' : chatformData.getLengthSync(),
								'Authorization': "bearer " + token
								}})
					.expectStatus(400)
					.expectHeaderContains('content-type','application/json')
					.inspectJSON()
					.toss();
				})
				.toss();
			})
		.toss();
		});
	});