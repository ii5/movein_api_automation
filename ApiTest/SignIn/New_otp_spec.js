var config = require('../../config');
var mobileList = require('../../Data/mobile');
var code = '';

describe('Generating New OTP for signin', function() {
	it('Request OTP by passing Mobile Number', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.expectJSON ( {
                "result":
                {
                    "code": '200',
                    "message": 'OTP Generated.',
                    "is_fatal": false
                },
                "data":
                {
                    "mobile_number": mobileList.mobile1,
                }
            })
            .expectJSONTypes (
            {
                "result":
                {
                    "code": String,
                    "message": String,
                    "is_fatal": Boolean
                },
                "data":
                {
                    "mobile_number": String,
                    "otp": String
                }
            })
        .toss();
    });
	it('Blank phone number for otp', function () {
		config.frisby.create ('Blank phone number field')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : "  "
					}
				},
				{json: true})
            .expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON ( {
                "result": {
						"code": "400",
						"message": "Value of 'mobile_number' is empty.",
						"is_fatal": true
						}
					})
               
            .expectJSONTypes (
            {
                "result":
                {
                    "code": String,
                    "message": String,
                    "is_fatal": Boolean
                }
            })
        .toss();
    });
	it('Invalid Format in Phone Number field for otp', function () {
		config.frisby.create ('Invalid phone number field')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : "+9198765467.8"
					}
				},
				{json: true})
            .expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON ( {
                "result": {
						"code": "400",
						"message": "Invalid value for property 'mobile_number'.",
						"is_fatal": true
						}
					})
               
            .expectJSONTypes (
            {
                "result":
                {
                    "code": String,
                    "message": String,
                    "is_fatal": Boolean
                }
            })
        .toss();
    });
    it('Invalid character in phone number field', function () {
		config.frisby.create ('Invalid character for Phone number field')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : "+919$%^&*(%^$"
					}
				},
				{json: true})
            .expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON ( {
                "result": {
						"code": "400",
						"message": "Invalid value for property 'mobile_number'.",
						"is_fatal": true
						}
					})
               
            .expectJSONTypes (
            {
                "result":
                {
                    "code": String,
                    "message": String,
                    "is_fatal": Boolean
                }
            })
        .toss();
    });
    it('Invalid Lenth in phone number field', function () {
		config.frisby.create ('Invalid Length for Phone number field')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : "+9193432423423423"
					}
				},
				{json: true})
            .expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON ( {
                "result": {
						"code": "400",
						"message": "Value of 'mobile_number' out of range.",
						"is_fatal": true
						}
					})
               
            .expectJSONTypes (
            {
                "result":
                {
                    "code": String,
                    "message": String,
                    "is_fatal": Boolean
                }
            })
        .toss();
    });
    it('Invalid String in phone number field', function () {
		config.frisby.create ('Invalid String for Phone number field')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : "........."
					}
				},
				{json: true})
            .expectStatus(400)
            .inspectJSON()
			.expectHeader('content-type','application/json')
			.expectJSON ( {
                "result": {
						"code": "400",
						"message": "Value of 'mobile_number' out of range.",
						"is_fatal": true
						}
					})
               
            .expectJSONTypes (
            {
                "result":
                {
                    "code": String,
                    "message": String,
                    "is_fatal": Boolean
                }
            })
        .toss();
    });
})