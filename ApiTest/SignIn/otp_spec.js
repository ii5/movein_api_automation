var config = require('../../config');
var formValues = { };
var signInFormValues = {};
var mobileList = require('../../Data/mobile');
var code = '';

describe('Generating New OTP for signin', function() {
	it('Request OTP by passing Mobile Number', function () {

		formValues.mobile_number = mobileList.mobile1;
		var form = {};
		form = config.makeFormData(formValues);
		var boundary = form.getBoundary();
        var lengthSync = form.getLengthSync();
        var mobile = formValues.mobile_number;

   
        config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',
            form,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + boundary,
                  'content-length': lengthSync
                }
            })
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.expectJSON ( {
                "result":
                {
                    "code": '200',
                    "message": 'OTP Generated.',
                    "is_fatal": false
                },
                "data":
                {
                    "mobile_number": mobile,
                }
            })
            .expectJSONTypes (
            {
                "result":
                {
                    "code": String,
                    "message": String,
                    "is_fatal": Boolean
                },
                "data":
                {
                    "mobile_number": String,
                    "otp": String
                }
            })
        .toss();
    });
	it('Blank phone number for otp ', function () {
        formValues.mobile_number = "";
		var form = {};
		form = config.makeFormData(formValues);

        config.frisby.create ('Blank Phone number')
        .post(config.url + '/request_otp',
            form,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                  'content-length': form.getLengthSync()
                }
            })
            .expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON ({
					"result": {
						"code": "MI-ERRVAL6002",
						"message": "Value of 'mobile_number' is empty.",
						"is_fatal": true
					}
				})
			.toss();
	});
	it('Invalid Format in Phone Number field for otp', function () {
		formValues.mobile_number = '+9198765467.8';
		var form = {};
		form = config.makeFormData(formValues);

        config.frisby.create ('Invalid Format for Phone Number')
        .post(config.url + '/request_otp',
            form,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                  'content-length': form.getLengthSync()
                }
            })
            .expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON ({
				"result": {
					"code": "MI-ERRVAL6006",
					"message": "Invalid value for property 'mobile_number'.",
					"is_fatal": true
					}
				})
			.toss();
	});
	it('Invalid character in phone number field for otp ', function () {
        formValues.mobile_number = '+91%$#^&>*({|';
		var form = {};
		form = config.makeFormData(formValues);
		config.frisby.create ('Invalid character Phone number')
		
        .post(config.url + '/request_otp',
            form,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                  'content-length': form.getLengthSync()
                }
            })
			.expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON ({
				"result": {
					"code": "MI-ERRVAL6006",
					"message": "Invalid value for property 'mobile_number'.",
					"is_fatal": true
					}
				})
		.toss();
	});
	it('invalid length in phone number field for otp', function () {
        formValues.mobile_number = '+91789456235895';
		var form = {};
		form = config.makeFormData(formValues);
		config.frisby.create ('invalid length Phone number')
		
        .post(config.url + '/request_otp',
            form,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                  'content-length': form.getLengthSync()
                }
            })
			
            .expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON({
				"result": {
					"code": "MI-ERRVAL6008",
					"message": "Value of 'mobile_number' out of range.",
					"is_fatal": true
				 	}
				})
		.toss();
	});
	it('invalid phone number in phone number field for otp ', function () {
		formValues.mobile_number = '=91jsdf';
		var form = {};
		form = config.makeFormData(formValues);
        config.frisby.create ('invalid Phone number')
		.post(config.url + '/request_otp',
            form,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                  'content-length': form.getLengthSync()
                }
            })
			
            .expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON({
					"result": {
						"code": "MI-ERRVAL6008",
						"message": "Value of 'mobile_number' out of range.",
						"is_fatal": true
						}
					})
		.toss();
	});

	it('Invalid string in phone number field for otp ', function () {
		formValues.mobile_number = '.......';
		var form = {};
		form = config.makeFormData(formValues);
		config.frisby.create ('Invalid String Phone number')
		
        .post(config.url + '/request_otp',
            form,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                  'content-length': form.getLengthSync()
                }
            })
			.expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON({
					"result": {
						"code": "MI-ERRVAL6008",
						"message": "Value of 'mobile_number' out of range.",
						"is_fatal": true
						}
					})
			.toss();
	});

	it('Invalid phone number and login attempt', function () {
		formValues.mobile_number = '+917208439238';
		var form = {};
		form = config.makeFormData(formValues);
		config.frisby.create ('OTP should Generate')
		
        .post(config.url + '/request_otp',
            form,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                  'content-length': form.getLengthSync()
                }
            })
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData) {
				code = jsonData.data.otp;
				signInFormValues = {
					mobile_number : '+917208439232',
					otp: code,
					grant_type: 'client_credentials',
					client_type: 'mobile',
					device_id: '1234',
					device_user_name: 'my device',
					device_model_name: 'iphone',
					device_platform: 'ios',
					device_os_version: 'ios8'
				};
				var signInFormData = {};
				signInFormData = config.makeFormData(signInFormValues);
				config.frisby.create ('Invalid phone number and login attempt')
				.post (config.url + '/sign_in',
					signInFormData,
						{
							json: false,
							headers: {
								'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
								'content-length': signInFormData.getLengthSync()
							}
						})
				.expectStatus(401)
				.expectJSON ({
						"result":{
						"code": String,
						"message": String,
						"is_fatal": Boolean
							},
				})
			.toss();
			})
		.toss();
    });

	it('Blank phone number field and attempt to login ', function () {
		formValues.mobile_number = '+917208439238';
		var form = {};
		form = config.makeFormData(formValues);
        config.frisby.create ('OTP should Generate')
		
        .post(config.url + '/request_otp',
            form,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
                  'content-length': form.getLengthSync()
                }
            })
        .expectStatus(200)
		.expectHeader('content-type','application/json')
		.afterJSON(function(jsonData) {
			code = jsonData.data.otp;
			signInFormValues = {
					otp: code,
					grant_type: 'client_credentials',
					client_type: 'mobile',
					device_id: '1234',
					device_user_name: 'my device',
					device_model_name: 'iphone',
					device_platform: 'ios',
					device_os_version: 'ios8'
			};
			var signInFormData = {};
			signInFormData = config.makeFormData(signInFormValues);
			config.frisby.create ('blank phone number field and attempt to login')
			.post (config.url+'/sign_in',
				signInFormData,
					{
						json: false,
						headers: {
							'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
							'content-length': signInFormData.getLengthSync()
						}
					}
			)
			.expectStatus(400)
			.expectJSON({
				"result":{
				"code": String,
				"message": String,
				"is_fatal": Boolean
				},
			})
			.toss();
		})
		.toss();
	});
	it('Error when the same OTP is reused', function () {
		formValues.mobile_number = '+917208439238';
		var form = {};
		form = config.makeFormData(formValues);
        config.frisby.create ('OTP should Generate')
		.post(config.url + '/movein-rest/v1/request_otp',
        form,
        {
            json: false,
            headers: {
              'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
              'content-length': form.getLengthSync()
            }
        })
        .expectStatus(200)
		.expectHeader('content-type','application/json')
		.afterJSON(function(jsonData) {
			code = jsonData.data.otp;
			signInFormValues = {
					mobile_number: '+917208439238',
					otp: code,
					grant_type: 'client_credentials',
					client_type: 'mobile',
					device_id: '1234',
					device_user_name: 'my device',
					device_model_name: 'iphone',
					device_platform: 'ios',
					device_os_version: 'ios8'
			};
			var signInFormData = {};
			signInFormData = config.makeFormData(signInFormValues);
			config.frisby.create ('Using OTP and successful login')
			.post (config.url+'/sign_in',
				signInFormData,
					{
						json: false,
						headers: {
							'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
							'content-length': signInFormData.getLengthSync()
						}
					}
			)
			.expectStatus(200)
			.toss();
		})
		.afterJSON(function(jsonData) {
			code = jsonData.data.otp;
			signInFormValues = {
					mobile_number: '+917208439238',
					otp: code,
					grant_type: 'client_credentials',
					client_type: 'mobile',
					device_id: '1234',
					device_user_name: 'my device',
					device_model_name: 'iphone',
					device_platform: 'ios',
					device_os_version: 'ios8'
			};
			var signInFormData = {};
			signInFormData = config.makeFormData(signInFormValues);
			config.frisby.create ('Using OTP and successful login')
			.post (config.url+'/sign_in',
				signInFormData,
					{
						json: false,
						headers: {
							'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
							'content-length': signInFormData.getLengthSync()
						}
					}
			)
			.expectStatus(400)
			.expectJSON({
				"result":{
				"code": String,
				"message": String,
				"is_fatal": Boolean
				},
			})
			.toss();
		});
	});
})