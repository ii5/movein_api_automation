var config = require('../../config');
var mobileList = require('../../Data/mobile');
var code ;

describe('Generating New OTP for signin And Than Checking Invalid Data For Signin', function() {
	it('Request OTP by passing Mobile Number And Use That Otp For Successful Signin', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Successful SignIn With Newly Generated Otp')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : mobileList.mobile1,
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(200)
			.expectHeader('content-type','application/json')
			.expectJSON	(
					{
						"result":
						{
							"code": String,
							"message": String,
							"is_fatal": Boolean
						},
						"data":
						{
							"initial_sync_index": Number,
							"push_channel": String,
							"push_key" : String,
							"access_token": String,
							"account_id": String,
							"scope" : String,
							"token_type":String
						}
					})
			.toss();
			})
        .toss();
    });
	it('Generating New Otp And Attempting Signin With Blank Phone Number Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Signin With Blank Phone Number Field')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : "    ",
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON (
			{
				"result":{
				"code": String,
				"message": String,
				"is_fatal": Boolean
				},
			})
			.toss();
			})
        .toss();
    });
	it('Generating New Otp And Attempting Signin With Invalid Phone Number Field (passing String)', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Signin With Invalid Phone Number Field (passing String)')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : "+91fg73478.&^",
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON (
			{
				"result":{
				"code": String,
				"message": String,
				"is_fatal": Boolean
				},
			})
			.toss();
			})
        .toss();
    });
	it('Generating New Otp And Attempting Signin With Invalid Phone Number Field (passing More Than 10 Digits)', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Signin With Invalid Phone Number Field (passing More Than 10 Digits)')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : "+91789456123569",
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON (
			{
				"result":{
				"code": String,
				"message": String,
				"is_fatal": Boolean
				},
			})
			.toss();
			})
        .toss();
    });
	it('Generating New Otp And Attempting Signin With Invalid Phone Number Field (passing Less Than 10 Digits)', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Signin With Invalid Phone Number Field (passing Less Than 10 Digits)')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number" : "+917894561",
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON (
			{
				"result":{
				"code": String,
				"message": String,
				"is_fatal": Boolean
				},
			})
			.toss();
			})
        .toss();
    });
	it('Generating New Otp And Attempting Signin With No Phone Number Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Signin With No Phone Number Field')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					 
					"otp": code,
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON (
			{
				"result":{
				"code": String,
				"message": String,
				"is_fatal": Boolean
				},
			})
			.toss();
			})
        .toss();
    });
	it('Generating New Otp And Attempting Signin With No Otp Field', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Signin With No Otp Field')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number": mobileList.mobile1,

					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON (
			{
				"result":{
				"code": String,
				"message": String,
				"is_fatal": Boolean
				},
			})
			.toss();
			})
        .toss();
    });
	it('Generating New Otp And Attempting Signin With Invalid Otp Field (Passing Alphanumeric)', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Signin With Invalid Otp Field (Passing Alphanumeric)')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number": mobileList.mobile1,
					"otp": "et&*",
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(400)
			.expectHeader('content-type','application/json')
			.expectJSON (
			{
				"result":{
				"code": String,
				"message": String,
				"is_fatal": Boolean
				},
			})
			.toss();
			})
        .toss();
    });
	it('Generating New Otp And Attempting Signin With Invalid Otp Field (Passing Wrong Otp)', function () {
		config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',{
			"request_otp":
					{
					"mobile_number" : mobileList.mobile1
					}
				},
				{json: true})
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.afterJSON(function(jsonData){
				code = jsonData.data.otp;
		config.frisby.create('Signin With Invalid Otp Field (Passing Wrong Otp)')
			.post(config.url + '/sign_in',{
				"sign_in" :
					{
					"mobile_number": mobileList.mobile1,
					"otp": "5678",
					"grant_type": "client_credentials",
					"client_type": "mobile",
					"device_id": "1234",
					"device_user_name": "my device",
					"device_model_name": "iphone",
					"device_platform": "ios",
					"device_os_version": "ios8"
					}
				},
				{json: true})
			.expectStatus(401)
			.expectHeader('content-type','application/json')
			.expectJSON (
			{
				"result":{
				"code": String,
				"message": String,
				"is_fatal": Boolean
				},
			})
			.toss();
			})
        .toss();
    });
	it('Generating New Otp And Attempting Signin With Blank Otp Field', function () {
			config.frisby.create ('OTP should Generate')
				.post(config.url + '/request_otp',{
				"request_otp":
						{
						"mobile_number" : mobileList.mobile1
						}
					},
					{json: true})
				.expectStatus(200)
				.expectHeader('content-type','application/json')
				.afterJSON(function(jsonData){
					code = jsonData.data.otp;
			config.frisby.create('Signin With Blank Otp Field')
				.post(config.url + '/sign_in',{
					"sign_in" :
						{
						"mobile_number": mobileList.mobile1,
						"otp": "  ",
						"grant_type": "client_credentials",
						"client_type": "mobile",
						"device_id": "1234",
						"device_user_name": "my device",
						"device_model_name": "iphone",
						"device_platform": "ios",
						"device_os_version": "ios8"
						}
					},
					{json: true})
				.expectStatus(400)
				.expectHeader('content-type','application/json')
				.expectJSON (
				{
					"result":{
					"code": String,
					"message": String,
					"is_fatal": Boolean
					},
				})
				.toss();
				})
			.toss();
		});
	it('Generating New Otp And Attempting Signin With Blank Grant_type Field', function () {
			config.frisby.create ('OTP should Generate')
				.post(config.url + '/request_otp',{
				"request_otp":
						{
						"mobile_number" : mobileList.mobile1
						}
					},
					{json: true})
				.expectStatus(200)
				.expectHeader('content-type','application/json')
				.afterJSON(function(jsonData){
					code = jsonData.data.otp;
			config.frisby.create('Signin With Blank Value In Grant_type Field')
				.post(config.url + '/sign_in',{
					"sign_in" :
						{
						"mobile_number": mobileList.mobile1,
						"otp": code,
						"grant_type": "  ",
						"client_type": "mobile",
						"device_id": "1234",
						"device_user_name": "my device",
						"device_model_name": "iphone",
						"device_platform": "ios",
						"device_os_version": "ios8"
						}
					},
					{json: true})
				.expectStatus(400)
				.expectHeader('content-type','application/json')
				.expectJSON (
				{
					"result":{
					"code": String,
					"message": String,
					"is_fatal": Boolean
					},
				})
				.toss();
				})
			.toss();
		});
	it('Generating New Otp And Attempting Signin With Invalid Value In Grant_type Field', function () {
			config.frisby.create ('OTP should Generate')
				.post(config.url + '/request_otp',{
				"request_otp":
						{
						"mobile_number" : mobileList.mobile1
						}
					},
					{json: true})
				.expectStatus(200)
				.expectHeader('content-type','application/json')
				.afterJSON(function(jsonData){
					code = jsonData.data.otp;
			config.frisby.create('Signin With Invalid Value In Grant_type Field')
				.post(config.url + '/sign_in',{
					"sign_in" :
						{
						"mobile_number": mobileList.mobile1,
						"otp": code,
						"grant_type": "43724898.3sd@#",
						"client_type": "mobile",
						"device_id": "1234",
						"device_user_name": "my device",
						"device_model_name": "iphone",
						"device_platform": "ios",
						"device_os_version": "ios8"
						}
					},
					{json: true})
				.expectStatus(400)
				.expectHeader('content-type','application/json')
				.expectJSON (
				{
					"result":{
					"code": String,
					"message": String,
					"is_fatal": Boolean
					},
				})
				.toss();
				})
			.toss();
		});
	it('Generating New Otp And Attempting Signin With No Grant_type Field', function () {
			config.frisby.create ('OTP should Generate')
				.post(config.url + '/request_otp',{
				"request_otp":
						{
						"mobile_number" : mobileList.mobile1
						}
					},
					{json: true})
				.expectStatus(200)
				.expectHeader('content-type','application/json')
				.afterJSON(function(jsonData){
					code = jsonData.data.otp;
			config.frisby.create('Signin With No Grant_type Field')
				.post(config.url + '/sign_in',{
					"sign_in" :
						{
						"mobile_number": mobileList.mobile1,
						"otp": code,
						
						"client_type": "mobile",
						"device_id": "1234",
						"device_user_name": "my device",
						"device_model_name": "iphone",
						"device_platform": "ios",
						"device_os_version": "ios8"
						}
					},
					{json: true})
				.expectStatus(400)
				.expectHeader('content-type','application/json')
				.expectJSON (
				{
					"result":{
					"code": String,
					"message": String,
					"is_fatal": Boolean
					},
				})
				.toss();
				})
			.toss();
		});
	it('Generating New Otp And Attempting Signin With Invalid Value In Client_type Field', function () {
			config.frisby.create ('OTP should Generate')
				.post(config.url + '/request_otp',{
				"request_otp":
						{
						"mobile_number" : mobileList.mobile1
						}
					},
					{json: true})
				.expectStatus(200)
				.expectHeader('content-type','application/json')
				.afterJSON(function(jsonData){
					code = jsonData.data.otp;
			config.frisby.create('Signin With Invalid Value In Client_type Field')
				.post(config.url + '/sign_in',{
					"sign_in" :
						{
						"mobile_number": mobileList.mobile1,
						"otp": code,
						"grant_type": "client_credentials",
						"client_type": "48$%nM",
						"device_id": "1234",
						"device_user_name": "my device",
						"device_model_name": "iphone",
						"device_platform": "ios",
						"device_os_version": "ios8"
						}
					},
					{json: true})
				.expectStatus(400)
				.expectHeader('content-type','application/json')
				.expectJSON (
				{
					"result":{
					"code": String,
					"message": String,
					"is_fatal": Boolean
					},
				})
				.toss();
				})
			.toss();
		});
	it('Generating New Otp And Attempting Signin With Blank In Client_type Field', function () {
			config.frisby.create ('OTP should Generate')
				.post(config.url + '/request_otp',{
				"request_otp":
						{
						"mobile_number" : mobileList.mobile1
						}
					},
					{json: true})
				.expectStatus(200)
				.expectHeader('content-type','application/json')
				.afterJSON(function(jsonData){
					code = jsonData.data.otp;
			config.frisby.create('Signin With Blank In Client_type Field')
				.post(config.url + '/sign_in',{
					"sign_in" :
						{
						"mobile_number": mobileList.mobile1,
						"otp": code,
						"grant_type": "client_credentials",
						"client_type": "   ",
						"device_id": "1234",
						"device_user_name": "my device",
						"device_model_name": "iphone",
						"device_platform": "ios",
						"device_os_version": "ios8"
						}
					},
					{json: true})
				.expectStatus(400)
				.expectHeader('content-type','application/json')
				.expectJSON (
				{
					"result":{
					"code": String,
					"message": String,
					"is_fatal": Boolean
					},
				})
				.toss();
				})
			.toss();
		});
	it('Generating New Otp And Attempting Signin With No Client_type Field', function () {
			config.frisby.create ('OTP should Generate')
				.post(config.url + '/request_otp',{
				"request_otp":
						{
						"mobile_number" : mobileList.mobile1
						}
					},
					{json: true})
				.expectStatus(200)
				.expectHeader('content-type','application/json')
				.afterJSON(function(jsonData){
					code = jsonData.data.otp;
			config.frisby.create('Signin With No Client_type Field')
				.post(config.url + '/sign_in',{
					"sign_in" :
						{
						"mobile_number": mobileList.mobile1,
						"otp": code,
						"grant_type": "client_credentials",

						"device_id": "1234",
						"device_user_name": "my device",
						"device_model_name": "iphone",
						"device_platform": "ios",
						"device_os_version": "ios8"
						}
					},
					{json: true})
				.expectStatus(400)
				.expectHeader('content-type','application/json')
				.expectJSON (
				{
					"result":{
					"code": String,
					"message": String,
					"is_fatal": Boolean
					},
				})
				.toss();
				})
			.toss();
		});
	it('Generating New Otp And Attempting Signin With Reusing The Same Otp Number', function () {
			config.frisby.create ('OTP should Generate')
				.post(config.url + '/request_otp',{
				"request_otp":
						{
						"mobile_number" : mobileList.mobile1
						}
					},
					{json: true})
				.expectStatus(200)
				.expectHeader('content-type','application/json')
				.afterJSON(function(jsonData){
					code = jsonData.data.otp;
			config.frisby.create('Signin With Blank Otp Field')
				.post(config.url + '/sign_in',{
					"sign_in" :
						{
						"mobile_number": mobileList.mobile1,
						"otp": code,
						"grant_type": "client_credentials",
						"client_type": "mobile",
						"device_id": "1234",
						"device_user_name": "my device",
						"device_model_name": "iphone",
						"device_platform": "ios",
						"device_os_version": "ios8"
						}
					},
					{json: true})
				.expectStatus(200)
				.expectHeader('content-type','application/json')
				.expectJSON (
				{
					"result":{
					"code": String,
					"message": String,
					"is_fatal": Boolean
					},
				})
			.afterJSON(function(jsonData){
			config.frisby.create('Signin With Reusing The Same Otp Number')
				.post(config.url + '/sign_in',{
					"sign_in" :
						{
						"mobile_number": mobileList.mobile1,
						"otp": code,
						"grant_type": "client_credentials",
						"client_type": "mobile",
						"device_id": "1234",
						"device_user_name": "my device",
						"device_model_name": "iphone",
						"device_platform": "ios",
						"device_os_version": "ios8"
						}
					},
					{json: true})
				.expectStatus(401)
				.expectHeader('content-type','application/json')
				.expectJSON (
				{
					"result":{
					"code": String,
					"message": String,
					"is_fatal": Boolean
					},
				})
			.toss();
			})
		.toss();
		})
	.toss();
	});
})