var config = require('../../config');
var formValues = { };
var signInFormValues = {};
var code = '';

describe('Generating New OTP for signin', function() {
	it('Invalid format of phone number and attempt to login', function () {
		formValues.mobile_number = '+917208439238';
		var form = {};
		form = config.makeFormData(formValues);
        config.frisby.create ('New OTP should Generate')
		.post(config.url + '/movein-rest/v1/request_otp',
        form,
        {
            json: false,
            headers: {
              'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
              'content-length': form.getLengthSync()
            }
        })
        .expectStatus(200)
		.expectHeader('content-type','application/json')
		.afterJSON(function(jsonData) {
			code = jsonData.data.otp;
			signInFormValues = {
					mobile_number: '+91720$#$%39238',
					otp: code,
					grant_type: 'client_credentials',
					client_type: 'mobile',
					device_id: '1234',
					device_user_name: 'my device',
					device_model_name: 'iphone',
					device_platform: 'ios',
					device_os_version: 'ios8'
			};
			var signInFormData = {};
			signInFormData = config.makeFormData(signInFormValues);
			config.frisby.create ('Using OTP and successful login')
			.post (config.url+'/sign_in',
				signInFormData,
					{
						json: false,
						headers: {
							'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
							'content-length': signInFormData.getLengthSync()
						}
					}
			)
			.expectStatus(401)
			.expectJSON ({
				"result":{
				"code": String,
				"message": String,
				"is_fatal": Boolean
				},
			})
			.toss();
		});
	});
it('Invalid format of phone number and attempt to login', function () {
		formValues.mobile_number = '+917208439238';
		var form = {};
		form = config.makeFormData(formValues);
        config.frisby.create ('New OTP should Generate')
		.post(config.url + '/movein-rest/v1/request_otp',
        form,
        {
            json: false,
            headers: {
              'content-type': 'multipart/form-data; boundary=' + form.getBoundary(),
              'content-length': form.getLengthSync()
            }
        })
        .expectStatus(200)
		.expectHeader('content-type','application/json')
		.afterJSON(function(jsonData) {
			code = jsonData.data.otp;
			signInFormValues = {
					mobile_number:'+91720843.238' ,
					otp: code,
					grant_type: 'client_credentials',
					client_type: 'mobile',
					device_id: '1234',
					device_user_name: 'my device',
					device_model_name: 'iphone',
					device_platform: 'ios',
					device_os_version: 'ios8'
			};
			var signInFormData = {};
			signInFormData = config.makeFormData(signInFormValues);
			config.frisby.create ('Using OTP and successful login')
			.post (config.url+'/sign_in',
				signInFormData,
					{
						json: false,
						headers: {
							'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
							'content-length': signInFormData.getLengthSync()
						}
					}
			)
			.expectStatus(401)
			.expectJSON ({
				"result":{
				"code": String,
				"message": String,
				"is_fatal": Boolean
				},
			})
			.toss();
		});
	});


	it('Successful login - Request OTP by passing Mobile Number and Use that OTP to signin ', function () {

		formValues.mobile_number = "+917208439238";
		var form = {};
		form = config.makeFormData(formValues);
		var boundary = form.getBoundary();
        var lengthSync = form.getLengthSync();
        var mobile = formValues.mobile_number;

   
        config.frisby.create ('OTP should Generate')
			.post(config.url + '/request_otp',
            form,
            {
                json: false,
                headers: {
                  'content-type': 'multipart/form-data; boundary=' + boundary,
                  'content-length': lengthSync
                }
            })
            .expectStatus(200)
			.expectHeader('content-type','application/json')
			.expectJSON ( {
                "result":
                {
                    "code": '200',
                    "message": 'OTP Generated.',
                    "is_fatal": false
                },
                "data":
                {
                    "mobile_number": mobile,
                }
            })
            .expectJSONTypes (
            {
                "result":
                {
                    "code": String,
                    "message": String,
                    "is_fatal": Boolean
                },
                "data":
                {
                    "mobile_number": String,
                    "otp": String
                }
            })
            .afterJSON(function(jsonData) {
				code = jsonData.data.otp;
				mobile = jsonData.data.mobile_number;
				signInFormValues = {
					mobile_number: mobile,
					otp: code,
					grant_type: 'client_credentials',
					client_type: 'mobile',
					device_id: '1234',
					device_user_name: 'my device',
					device_model_name: 'iphone',
					device_platform: 'ios',
					device_os_version: 'ios8'
			};
			var signInFormData = {};
			signInFormData = config.makeFormData(signInFormValues);
				config.frisby.create ('Successful login')
				.post (config.url+'/sign_in',
				signInFormData,
					{
						json: false,
						headers: {
							'content-type': 'multipart/form-data; boundary=' + signInFormData.getBoundary(),
							'content-length': signInFormData.getLengthSync()
						}
					}
				)
				.expectStatus(200)
				.expectJSON
				(
					{
						"result":
						{
							"code": String,
							"message": String,
							"is_fatal": Boolean
						},
						"data":
						{
							"initial_sync_index": Number,
							"push_channel": String,
							"push_key" : String,
							"access_token": String,
							"account_id": String,
							"scope" : String,
							"token_type":String
						}
					})
			.toss();
			})
        .toss();
    });
});